<h1>Google Summer of Code Final Report: The GJS Developer Experience</h1>

*Note: My username appears as 'Evan Welsh' or 'rockon999' depending on the development platform.*

This is my final report for Google Summer of Code and a public list of all my work throughout the summer.

- [Summary](#summary)
- [What I've Done](#what-ive-done)
    - [GJS Sandbox](#gjs-sandbox)
    - [gjs.guide](#gjsguide)
    - [DevDocs](#devdocs)
    - [GObject Introspection](#gobject-introspection)
    - [GJS](#gjs)
    - [Builder](#builder)
    - [App Template](#app-template)
- [Reflection](#reflection)
- [What's Left](#whats-left)
- [Credits](#credits)

## Summary

For Google Summer of Code, I worked on a multi-faceted project to improve the development experience for GJS, the JavaScript bindings for GNOME. I created a sample application, built a collaborative website with tips and tutorials, worked on new build environments for GJS, and improved the existing documentation.

## What I've Done

### GJS Sandbox

I created GJS Sandbox, a new GTK+ application built with GJS and packaged using flatpak and Meson. GJS Sandbox allows users to run snippets of GJS code in a sandboxed environment with the latest GJS version. GJS Sandbox eliminates the need for teaching beginners build scripts, version incompatibilities, or application frameworks and allows anyone to jump into GJS quickly.

**You can find its codebase [here](https://gitlab.gnome.org/rockon999/sandbox).**

I created GJS Sandbox to provide a functional, "sample" application for GJS. I was able to utilize my existing knowledge and also test my [tutorial](#gjs.guide).

### gjs.guide

[gjs.guide](http://gjs.guide) is an open source, community-oriented website that provides in-depth tutorials and examples of GJS applications. It is based on [Vuepress](https://vuepress.vuejs.org/) with custom theming and components added. For gjs.guide I wrote an extension tutorial on GTK+ for GJS and several smaller tutorials guiding the user through the basics of GJS.

The written tutorials can be found here:

**[Tutorials](http://gjs.guide/guides/)**

While the site, in general, is found here:

**[gjs.guide](http://gjs.guide/)**

*The website source can be accessed by selecting `GitLab` in the site's navigation bar.*

Working on [gjs.guide](http://gjs.guide), I gained insight into GJS' GTK+ integration and improved my ability to help new programmers and community members utilize GJS in their work.

### DevDocs

In 2015, Philip Chimento forked DevDocs, an API reference aggregator, to provide support for GJS and GNOME libraries. He revised the codebase in 2017, but the codebase required updating due to upstream changes.

I went through three iterations of DevDocs forks.

*1st iteration:*
- Goals:
    - Make the repository buildable and introduce a GNOME-themed interface.
- Results: 
    - Created a somewhat drastic fork of upstream
    - Implemented a very GNOME-centric user interface
- Takeaways:
    - Helpful in navigating the codebase
    - Large modifications to upstream user interface files can lead to maintainability issues

*2nd Iteration:*
- Goals:
    - Improve backend and frontend code of the 1st iteration
- Results:
    - Fixed XML parsing errors
    - Diagnosed errors in the scraping stack
    - Fixed UI bugs
    - Ported frontend to ES6 from CoffeeScript
- Takeaways:
    - A much more reliable application, but maintainability issues still creep in from 1st iteration changes
  
*3rd Iteration:*
- Goals:
    - Eliminate maintainability issues
    - Incorporate key features of 1st and 2nd iterations
- Result:
    - A stable, minimal fork of upstream with GNOME features and limited maintenance required.
- Takeaways:
    - The hybrid of upstream, new code, and features from the 1st and 2nd iterations resulted in a stable, robust application.

**You can find iterations one [here](https://gitlab.gnome.org/rockon999/gjs-devdocs/) and two [here](https://gitlab.gnome.org/rockon999/gjs-devdocs/tree/frontend).**

**You can find the final code base [here](https://github.com/rockon999/devdocs-es6/tree/gnome-compat). It is being merged into production from the `rebase-evan-in-progress` branch to `gnome-es6` [here](https://github.com/ptomato/devdocs/)**

Overall, working on DevDocs I gained insight into Ruby and CoffeeScript, how they interact with web frameworks, and how Git maintainability impacts web applications.

### GObject Introspection

I updated and extended Philip Chimento’s initial work from 2017 on a devdocs output format for `g-ir-doc-tool` (DevDocs depends on `g-ir-doc-tool` to generate GJS documentation).

- I fixed a bug which allowed functions not actually appearing in GJS to creep into the documentation
- I provided improved index files which sorted the documentation by type (class, enum, etc).
- I fixed errors in the GTK+ markdown processing which broke some libraries' documentation
- I ensured compatibility with changes that had occurred since the format was initially proposed.

**You can find the current merge request for this work [here](https://gitlab.gnome.org/GNOME/gobject-introspection/merge_requests/57). It is expected to enter the `master` branch for the next release of gobject-introspection.**

### GJS

I exposed static class properties and fixed a UTF-8 encoding bug with the GJS scripting shell.

Exposing static class properties allows users to define critical GObject-related properties without using the helper API. This provides support for the future ES7+ syntax of [class fields](https://github.com/tc39/proposal-class-fields).

I also created a build script with Meson which allows anyone to utilize Babel without outside dependencies.

**The merge request for the static class properties fixes is [here](https://gitlab.gnome.org/GNOME/gjs/merge_requests/197).**

**The merge request for UTF-8 encoding fixes is [here](https://gitlab.gnome.org/GNOME/gjs/merge_requests/108).**

**The babel template for GJS can be found [here](https://gitlab.gnome.org/rockon999/gjs-babel-template).**

### Builder

I implemented fixes for the default GJS GTK+ Meson application template. The fixes updated GNOME Builder’s templates to ES6 syntax and added style fixes.

**You can find the merge request [here](https://gitlab.gnome.org/GNOME/gnome-builder/merge_requests/87/).**

My work on GNOME Builder and the [App Template](#app-template) helped me improve the tutorials for [gjs.guide](http://gjs.guide).

### App Template

In conjunction with GNOME Builder, I also created a new GJS GTK+ template repository for users of IDEs other than GNOME Builder.

**You can find that repository [here](https://gitlab.gnome.org/rockon999/gjs-gtk-template).**

## Reflection

I changed several tasks over the project with input from my mentors. I decided the sample application ideas in the proposal, GNOME Tags and TicTacToe, were not conducive to a "tutorial"-style application. I created  GNOME Sandbox as the new sample application for the project and I reoriented the tutorials to ensure they were applicable to the greatest amount of GNOME developers. I believe these adjustments improved the final project.

I contributed to projects such as [GNOME Builder](#gnome-builder) that were not initially part of the proposal as it became clear they were integral to the tutorials and experience I was building.

While I have worked with GJS for many years, this project exposed me to the internals of the language which I only rarely visited prior to GSoC.

It was exciting and a great learning experience to finally contribute to a project I have used for many years!

## What's Left

I did a lot of work over the summer, but much remains to continue improving the GJS experience. Below is a non-exhaustive list of those tasks.

- More sample applications
    - While GJS Sandbox is an excellent sample, the existing sample library for GJS has fallen out-of-date and the project needs more work in this sector.
- Moving DevDocs work upstream
   - The work moving some of our DevDocs changes and improvements upstream has already begun but will likely take a few more months of collaboration with DevDocs.io (an upstream, outside group) to merge.
- GJS Sandbox needs to be moved to Flathub
   - It would be ideal for GJS Sandbox to live on Flathub instead of as an application bundle.
- GObject Introspection still has custom Markdown parsing code which should be moved to Python Markdown extensions.
   - The scope of this project didn't necessitate this, but it would be ideal for performance and maintainability.
- More tutorials for [gjs.guide](http://gjs.guide)!
   - The community will play a large part in this, but to make the GJS Guide successful we will need a large body of work for users to peruse.

## Credits

- Thank you to Manuel Quiñones and Philip Chimento, my amazing mentors! Your tireless hours helping me find resources, fix bugs, and review code were invaluable.
- Thank you to the GNOME Foundation for sponsoring my first GUADEC; it was a truly amazing experience.
- Thank you to all the upstream projects who've reviewed my changes and gave feedback.
- And, finally, thank you to Google for the Summer of Code program.